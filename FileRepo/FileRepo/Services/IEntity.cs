﻿using System;
namespace FileRepo
{
	public interface IEntity
	{
		int Id { get; set; }
	}
}
