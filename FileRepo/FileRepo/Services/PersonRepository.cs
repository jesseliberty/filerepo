﻿using System;
namespace FileRepo
{
	public class PersonRepository : GenericFileRepository<Person>
	{
		public PersonRepository () : base ("PersonFile.json") { }
	}
}
