﻿using System;
using Xamarin.Forms;
using System.Linq;
using System.Collections.Generic;

namespace FileRepo
{
	public partial class FileRepoPage : ContentPage
	{
		private List<Person> people = new List<Person> ();
		private static int ids = 0;

		public FileRepoPage ()
		{
			InitializeComponent ();

		}

		public void OnAdd (object o, EventArgs e)
		{
			var person = new Person ();
			person.Id = ids++;
			person.Name = PersonName.Text;
			PersonName.Text = string.Empty;
			people.Add (person);

		}

		public void OnNextPage (object o, EventArgs e)
		{
			Navigation.PushAsync (new PeopleListPage (people), true);

		}
	}
}
