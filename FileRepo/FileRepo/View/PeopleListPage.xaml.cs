﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;

namespace FileRepo
{
	public partial class PeopleListPage : ContentPage
	{
		public ObservableCollection<Person> People { get; set; } = new ObservableCollection<Person> ();

		public PeopleListPage (List<Person> people)
		{
			foreach (Person person in people) {
				People.Add (person);
			}
			InitializeComponent ();
			this.BindingContext = this;
		}

		public void OnStore (object o, EventArgs e)
		{
			var repo = new PersonRepository ();
			repo.Save (People);
		}

		public void OnRestore (object o, EventArgs e)
		{
			var repo = new PersonRepository ();
			var people = repo.GetAll ();
			foreach (Person person in people) {
				People.Add (person);
			}
		}
	}
}
