﻿using System;
using System.IO;
using FileRepo.Droid;
using Xamarin.Forms;

[assembly: Dependency (typeof (FileImplementation))]
namespace FileRepo.Droid
{
	public class FileImplementation : IFile
	{
		public void SaveText (string filename, string text)
		{
			var documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
			var filePath = Path.Combine (documentsPath, filename);
			File.Delete (filePath);
			File.WriteAllText (filePath, text);

		}
		public string LoadText (string filename)
		{
			var documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
			var filePath = Path.Combine (documentsPath, filename);
			return System.IO.File.ReadAllText (filePath);
		}

		public void ClearFile (string filename)
		{
			var documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
			var filePath = Path.Combine (documentsPath, filename);
			File.Delete (filePath);

		}

		public bool FileExists (string filename)
		{
			var documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
			var filePath = Path.Combine (documentsPath, filename);

			return File.Exists (filePath);
		}
	}
}
